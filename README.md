![NPM License](https://img.shields.io/npm/l/gcrypto)
![NPM Downloads](https://img.shields.io/npm/dw/gcrypto)
![NPM Downloads](https://img.shields.io/gitlab/pipeline-status/gshohat/gcrypto?branch=main)

# gcrypto

Lightweight Nodejs app that sends emails once a symbol price(ada/usd for example) is above a threshold.
web socket client listens to Binance feeds.
Using send grid api to send emails with the text:
'price:1.51500000, date:11/28/2021, 7:06:35 AM'

## Getting started
npm i

npm run serve 

create .env file in root folder
```
SYMBOL=adausdt
THRESHOLD=1.6
TO=gcrypto@gmail.com
FROM=gcrypto@giladshohat.com
SUBJECT=ADA is up!
SENDGRID_API_KEY= //add
```

## Contact
contact@giladshohat.com
