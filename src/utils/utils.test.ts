import {isPriceAbove} from "./utils";

test('price above threshold', () => {
    const payload = {
        T: 123,
        p: "2.1",
    };
    const threshold = 2;
    const res = isPriceAbove(payload, threshold);
    expect(res).toBe(true);
});

