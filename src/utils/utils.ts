import {IPayload} from "../payload";

export function isPriceAbove(payload: IPayload, threshold: number) {
    const price = parseFloat(payload.p);
    return price > threshold;
}
