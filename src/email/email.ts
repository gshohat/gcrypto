import sgMail from '@sendgrid/mail';
import {IPayload} from "../payload";

export const sendEmail = async(payload: IPayload) => {
    if (!validProcessEnvs()) {
        throw new Error('not valid env');
    }

    const env = process.env;
    sgMail.setApiKey(env.SENDGRID_API_KEY!);
    const date = new Date(payload.T).toLocaleString();
    const msg = {
        to: env.TO!,
        from: env.FROM!,
        subject: env.SUBJECT!,
        html: `<span>price:${payload.p}, date:${date}</span>`,
    };
    return sgMail.send(msg);
};

export function validProcessEnvs(): boolean {
    const {SENDGRID_API_KEY, TO, FROM, SUBJECT} = process.env;
    const env = [SENDGRID_API_KEY, TO, FROM, SUBJECT];
    return env.every((el) => {
        return el !== undefined;
    });
}
