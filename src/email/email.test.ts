import {sendEmail, validProcessEnvs} from "./email";
import dotenv from "dotenv";
import {ResponseError} from "@sendgrid/helpers/classes";

test('not all process env valid - return false', () => {
    dotenv.config({ path: './.jest/not-valid-process.env' });
    const res = validProcessEnvs();
    expect(res).toEqual(false);
});

test('mail not sent - not valid env', async() => {
    dotenv.config({ path: './.jest/not-valid-process.env' });
    try {
        await sendEmail({p:'1.7', T:123});
    } catch(err) {
        if (err instanceof Error) {
            expect(err.message).toEqual('not valid env');
        }
    }
});

test('mail sent - success',  async() => {
    jest.setTimeout(8000);
    dotenv.config({ path: './.jest/valid-process.env' });
    try {
        const res = await sendEmail({p:'1.7', T:123});
        expect(res[0].statusCode).toEqual(202);
    } catch (err) {
        if (err instanceof ResponseError) {
            const errors = JSON.stringify(err.response.body);
            throw new Error(errors);
        }
    }

});

test('all process env valid - return true', () => {
    dotenv.config({ path: './.jest/valid-process.env' });
    const res = validProcessEnvs();
    expect(res).toEqual(true);
});
