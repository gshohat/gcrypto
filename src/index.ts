import WebSocket from 'ws';
import {sendEmail} from "./email/email";
import dotenv from "dotenv";
import {isPriceAbove} from "./utils/utils";
import {IPayload} from "./payload";

dotenv.config();

const ws = new WebSocket(`wss://stream.binance.com:9443/ws/${process.env.SYMBOL}@trade`);

ws.on('message', async (message) => {
    const payload: IPayload = JSON.parse(message.toString());
    if (!process.env.THRESHOLD) {
        console.log('threshold not defined');
        ws.close();
    }
    if (!process.env.SYMBOL) {
        console.log('symbol is not defined');
        ws.close();
    }
    const threshold = parseFloat(process.env.THRESHOLD!);
    if (isPriceAbove(payload, threshold)) {
        console.log('sent email');
        console.log(`p:${payload.p}, T:${payload.T}`);
        await sendEmail(payload);
    }
});
