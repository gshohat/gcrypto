module.exports = async () => {
    return {
        roots: ['<rootDir>/src'],
        preset: 'ts-jest',
        verbose: false,
    };
};
